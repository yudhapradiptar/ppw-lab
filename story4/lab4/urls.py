from django.urls import re_path, path
from .views import index,CV,funFact,form,post_schedule,formJadwal
#url for app
urlpatterns = [
    re_path(r'personalWeb/', index, name='index'),
    re_path(r'CV/', CV, name='CV'),
    re_path(r'funFact/', funFact, name='funFact'),
    re_path(r'form/', form, name='form'),
	re_path(r'formJadwal/', formJadwal, name='formJadwal'),
	re_path(r'post_schedule/', post_schedule, name='post_schedule')
]
