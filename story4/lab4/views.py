from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Jadwal
from .forms import *
from django.shortcuts import render
response = {'author' : "Yudha" }

def index(request):
	return render(request, 'personalWeb.html', {})
def CV(request):
	return render(request, 'CV.html', {})
def funFact(request):
	return render(request, 'funFact.html', {})
def form(request):
	return render(request, 'form.html', {})
def formJadwal(request):
	response['schedule_form'] = Jadwal_form
	return render(request, 'formJadwal.html', response)
def post_schedule(request):
	form = Jadwal_form(request.POST or None)
	if (request.method == 'POST' and 'submit' in request.POST):
		response['nama_acara'] = request.POST['nama_acara']if request.POST['nama_acara']!="" else "Anonymous"
		response['tanggal_acara'] = request.POST['tanggal_acara']if request.POST['tanggal_acara']!="" else "Anonymous"
		response['tempat_acara'] = request.POST['tempat_acara']if request.POST['tempat_acara']!="" else "Anonymous"
		response['kategori_acara'] = request.POST['kategori_acara']
		jadwal = Jadwal(nama_acara=response['nama_acara'], tanggal_acara=response['tanggal_acara'], tempat_acara=response['tempat_acara'], kategori_acara=response['kategori_acara'])
		jadwal.save()
		pushjadwal = Jadwal.objects.all()
		response['jadwalacara'] = pushjadwal
		jadwalhtml = 'schedule.html'
		return render(request, jadwalhtml, response)
	elif (request.method == 'POST' and 'delete' in request.POST):
		jadwalhtml = 'schedule.html'
		pushjadwal = Jadwal.objects.all()
		response['jadwalacara'] = pushjadwal.delete()
		return render(request, jadwalhtml, response)
	else:
		return HttpResponseRedirect('/formJadwal/')
		

# Create your views here.
