from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Jadwal(models.Model):
	nama_acara = models.CharField(max_length=200)
	tanggal_acara = models.DateTimeField()
	tempat_acara = models.CharField(max_length=200)
	kategori_acara = models.CharField(max_length=200)

	def __str__(self):
		return self.nama_acara
