from django import forms
from .models import *
class DateTimeInput(forms.DateTimeInput):
	input_type = 'datetime-local'

class Jadwal_form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    nama_acara = forms.CharField(label = 'Nama Acara', required=True, widget=forms.TextInput(attrs=attrs), max_length=15)
    tanggal_acara = forms.DateTimeField(label = 'Tanggal Acara', required=True, widget=DateTimeInput(attrs=attrs), input_formats = '%m/%d/%Y %H:%M')
    tempat_acara = forms.CharField(label = 'Tempat Acara', required=True, widget=forms.TextInput(attrs=attrs), max_length=15)
    kategori_acara = forms.CharField(label = 'Kategori Acara', required=False, widget=forms.TextInput(attrs=attrs), max_length=15)
    class Meta:
    	model = Jadwal
    
