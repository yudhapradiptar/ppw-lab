from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Yudha Pradipta Ramadan' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 1, 5) #TODO Implement this, format (Year, Month, Date)
npm = 1706043424 # TODO Implement this
university = 'Universitas Indonesia'
hobby = 'watching football'
bio = 'I love laying in bed all day and watching football'
fname = 'Ariq Naufal Satria'
fbirth_date = date(1999, 9, 28)
fnpm = 1706027414
funiversity = 'Gedung bundar nama nya pacil'
fhobby = 'Desain grafis pake power po*nt'
fbio= 'Anak normal kelahiran kota hujan yang sekarang sedang merantau ke kota sebelah dengan kereta commuter line.'
bname = 'Yosua Krisnando Bagaskara'
bbirth_date = date(1998, 12, 16)
bnpm = 1706039591
buniversity = 'Universitas Indonesia'
bhobby = 'Singing, Dancing, Reading, and Sleeping'
bbio = 'I like Singing, Dancing, Reading, and Sleeping'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm , 'university': university, 'hobby': hobby, 'bio': bio, 
	'front_name': fname, 'front_age': calculate_age(fbirth_date.year), 'front_npm': fnpm, 'front_university': funiversity, 'front_hobby': fhobby, 'front_bio': fbio, 
	'behind_name': bname, 'behind_age': calculate_age(bbirth_date.year), 'behind_npm': bnpm, 'behind_university': buniversity, 'behind_hobby': bhobby, 'behind_bio': bbio}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
