"""lab6projectTDD URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
    hahahaha
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.conf.urls import include
from applab6TDD.views import index as index



urlpatterns = [
    re_path('admin/', admin.site.urls),
    re_path('', include('applab6TDD.urls')),
    re_path(r'^$', index, name='index'),

]
