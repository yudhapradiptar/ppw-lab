from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class PageTestCase(TestCase):
	def test_url_lab6(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)
	
	def test_hello_apa_kabar(self):
		request = HttpRequest()
		response = self.client.get('/')
		self.assertContains(response, 'Hello Apa Kabar')

	def setUp_status(self):
		return StatusModels.objects.create(title="Sedih", description="Sedang sedih karena tugas numpuk")

	def test_status_is_valid(self):
		new_status = self.setUp_status()
		self.assertEqual(new_status.title, 'Sedih')

	def test_model_can_create_new_status(self):
		new_status = StatusModels.objects.create(title='mengerjakan lab ppw', description='mengerjakan lab 6 ppw yuhuu')
		counting_all_available_status = StatusModels.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)


class PageFunctionalTest(TestCase):
    
    def setUp(self):
    	chrome_options = Options()
    	chrome_options.add_argument('--dns-prefetch-disable')
    	chrome_options.add_argument('--no-sandbox')
    	chrome_options.add_argument('--headless')
    	self.selenium = webdriver.Chrome(chrome_options=chrome_options)
    	super(PageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(PageFunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        title = selenium.find_element_by_name('title')
        description = selenium.find_element_by_name('description')

        submit = selenium.find_element_by_name('submit')

        # Fill the form with data
        title.send_keys('Mengerjakan Lab PPW')
        description.send_keys('Lab kali ini membahas tentang functional test')

        # submitting the form
        submit.send_keys(Keys.RETURN)

        # cek hasil
        self.assertIn('Mengerjakan Lab PPW', self.selenium.page_source)
    #style
    def test_hello_apa_kabar(self):
    	selenium = self.selenium
    	selenium.get('http://127.0.0.1:8000/')
    	#nge test warna dari tulisan Hello Apa Kabar
    	h1= selenium.find_element_by_tag_name('h1').value_of_css_property("color")
    	self.assertEquals('rgba(0, 0, 255, 1)', h1)
    
    #style
    def test_navbar(self):
    	selenium = self.selenium
    	selenium.get('http://127.0.0.1:8000/')
    	#nge test apakah warna dari nav nya
    	nav= selenium.find_element_by_tag_name('nav').value_of_css_property("background-color")
    	self.assertEquals('rgba(0, 0, 255, 1)', nav)

    #layout
    def test_hello_apa_kabar_layout(self):
    	selenium = self.selenium
    	selenium.get('http://127.0.0.1:8000/')
    	assert selenium.page_source.find("Hello Apa Kabar")

    #layout
    def test_judul_status_layout(self):
    	selenium = self.selenium
    	selenium.get('http://127.0.0.1:8000/')
    	assert selenium.page_source.find("Judul Status")




		



# Create your tests here.123
