from django import forms
from .models import *
class DateTimeInput(forms.DateTimeInput):
	input_type = 'datetime-local'

class StatusForm(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    title = forms.CharField(label = 'Title', required=True, widget=forms.TextInput(attrs=attrs), max_length=20)
    description = forms.CharField(label = 'Description', required=False, widget=forms.Textarea(attrs=attrs), max_length=300)
    class Meta:
    	model = StatusModels
    
