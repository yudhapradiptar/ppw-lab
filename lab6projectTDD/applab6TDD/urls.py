from django.urls import re_path, path
from .views import index, post_status

urlpatterns = [
    re_path(r'homePage/', index, name='index'),
    re_path(r'post_status/', post_status, name='post_status'),
]
